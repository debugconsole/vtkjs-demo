// @ts-nocheck
/* eslint-disable */

import HEX from "./HEX.js";

var ORAlAnalysis = {};
var analysisNum = 0;

ORAlAnalysis.showAnalysis = function(hex) {
  var data = {};
  var srw=  ORAlAnalysis.hexAnalysis(hex, "double")
  var m_SpeedPercent =(srw); //步长

  data.m_SpeedPercent = m_SpeedPercent;
  var srw1 =   ORAlAnalysis.hexAnalysis(hex, "int")
  var nodeSize = (srw1); //节点数
  data.nodeSize = nodeSize;
  var toothNum = 0; //牙齿数
  if (nodeSize > 0) {
      var srw2 =   ORAlAnalysis.hexAnalysis(hex, "int")
      toothNum = srw2;
      data.toothNum = toothNum;
  }
  console.info(hex);
  var stepindexs = new Array();

  ORAlAnalysis.step_index = function(){
      var stepindex;
      var nodeType;
      var tooth;
  }
  for (let i = 0; i < data.nodeSize; i++) {
      var step_indext = new ORAlAnalysis.step_index();
      var srw3 =   ORAlAnalysis.hexAnalysis(hex, "int")
      var stepindex =(srw3);//节点编号
      step_indext.stepindex = stepindex
     var tooth = new Array();
    
      for (let i = 0; i < data.toothNum; i++) {
          var srw4 =   ORAlAnalysis.hexAnalysis(hex, "int")
          var toothid = (srw4); //牙齿id
          var materix = [];
          for (let n = 0; n < 16; n++) {
              materix[n] = (ORAlAnalysis.hexAnalysis(hex, "double")); //牙齿矩阵
          }
           tooth[toothid]= materix;
      }
      var nodeType = (ORAlAnalysis.hexAnalysis(hex, "int")); //节点类型
      step_indext.nodeType =nodeType;
      step_indext.tooth =tooth;
      stepindexs[stepindex] =step_indext;
  }
  data.stepindexs = stepindexs;

  var m_posPace = (ORAlAnalysis.hexAnalysis(hex, "double")); //总平移步长
     var m_degreePace = (ORAlAnalysis.hexAnalysis(hex, "double")); //总旋转步长
     data.m_posPace =m_posPace;
     data.m_degreePace =m_degreePace;
    var posPace=[];
    var degreePace=[];
     for (let i = 0; i < data.nodeSize; i++) {
      posPace[i] = (ORAlAnalysis.hexAnalysis(hex, "double")); //节点平移步长
      degreePace[i] = (ORAlAnalysis.hexAnalysis(hex, "double")); //节点旋转步长
      
      }
      data.posPace =posPace;
      data.degreePace =degreePace;

   var m_DownArchMatrix =[];
   for (let i = 0; i < 16; i++) {
      m_DownArchMatrix[i] =(ORAlAnalysis.hexAnalysis(hex, "double")); //下颌移动矩阵
   }
   data.m_DownArchMatrix= m_DownArchMatrix;
  analysisNum = 0;
  return data;
}

ORAlAnalysis.showAnalysisMesh=function (hex){
  let data = {};
  let cNumberOfPoints=  ORAlAnalysis.hexAnalysis(hex, "longint");
  data.cNumberOfPoints = cNumberOfPoints;
  let cNumberPointPos =[];

  for(let i=0;i<data.cNumberOfPoints;i++){
      let pointPos=[];
      for(let l=0;l<3;l++){
          pointPos[l]=ORAlAnalysis.hexAnalysis(hex, "double");
      }
      cNumberPointPos[i] =pointPos;
  }

  data.cNumberPointPos =cNumberPointPos;
 let cNumberOfCells =ORAlAnalysis.hexAnalysis(hex, "longint");
 data.cNumberOfCells = cNumberOfCells;
 let cNPoints={}

 for(let i=0;i<data.cNumberOfCells;i++){
 let  cNPoint= ORAlAnalysis.hexAnalysis(hex, "longint");
  let cPts=[];
  for(let l=0;l<cNPoint;l++){
      cPts[l] =ORAlAnalysis.hexAnalysis(hex, "longint");
  }
  cNPoints[cNPoint] =cPts;
 }

 data.cNPoints =cNPoints;

let toothLineSize = ORAlAnalysis.hexAnalysis(hex, "int");

data.toothLineSize =toothLineSize;
let cNumberOfToothLine ={};

for(let i=0;i<data.toothLineSize;i++){
 let cNumberToothLine =ORAlAnalysis.hexAnalysis(hex, "longint");
 let cPointId=[];
  for(let l=0;l<cNumberToothLine;l++){
      cPointId[l]=ORAlAnalysis.hexAnalysis(hex, "longint");
  }
  let  cNumberGum1 =ORAlAnalysis.hexAnalysis(hex, "longint");
  let cPointId1 =[];
  for(let l=0;l<cNumberGum1;l++){
      cPointId1[l]=ORAlAnalysis.hexAnalysis(hex, "longint");
  }
  let  cNumberGum2 =ORAlAnalysis.hexAnalysis(hex, "longint");
  let cPointId2 =[];
  for(let l=0;l<cNumberGum2;l++){
      cPointId2[l]=ORAlAnalysis.hexAnalysis(hex, "longint");
  }
  cNumberOfToothLine.cNumberToothLine[cNumberToothLine] =cPointId;
  cNumberOfToothLine.cNumberToothLine[cNumberGum1] =cPointId1;
  cNumberOfToothLine.cNumberToothLine[cNumberGum2] =cPointId2;
}

data.cNumberOfToothLine=cNumberOfToothLine;
console.info(":analysisNum:"+analysisNum);
analysisNum = 0;
return data;
}

ORAlAnalysis.showAnalysisATM = function(hex,archType) {
  let start=0;
  let end=0;
  if(archType=="up")
  {
      start = 0;
      end = 17;
  }
  else if(archType=="down")
  {
      start = 17;
      end = 33;
  }
  
  let data = {};
  let version =ORAlAnalysis.hexAnalysis(hex, "int");
  data.version = version;

  let tooths= new Array();
  for(let v=start;v<end;v++)
  {
      let tooth={};
      let toothid =  ORAlAnalysis.hexAnalysis(hex, "int");//牙id
      tooth.toothid= toothid;
      let size =  ORAlAnalysis.hexAnalysis(hex, "int");//附件数量
      tooth.size= size;
      let attachments=[];
      for(let i=0;i<tooth.size;i++)
      {
          let attachment={};
          let  AttachmentId = ORAlAnalysis.hexAnalysis(hex, "int");//附件id
          let  attachStep = ORAlAnalysis.hexAnalysis(hex, "int");//第几步开始添加
          let  removeStep = ORAlAnalysis.hexAnalysis(hex, "int");//第几步开始移除
          attachment.AttachmentId =AttachmentId;
          attachment.attachStep=attachStep;
          attachment.removeStep =removeStep;
          attachment.Property =0;
          attachment.xAngle =0;
          attachment.pos=[];
          attachment.ros=[];
          attachment.scale=[];
          if( data.version>2)
          {  
          let Property =ORAlAnalysis.hexAnalysis(hex, "int");//附件类型
          attachment.Property =Property;
          }
          if( data.version==5)
          {
              let  xAngle =ORAlAnalysis.hexAnalysis(hex, "double");//旋转值？
              attachment.xAngle =xAngle;
          }
          if( data.version>5){
              let pos=[];
              for(let l=0;l<16;l++){
                  pos[l] =ORAlAnalysis.hexAnalysis(hex, "double");//坐标
              }
              attachment.pos =pos;
          }
          let ros=[];                                                          
          for(let x=0;x<16;x++){
              ros[x] =ORAlAnalysis.hexAnalysis(hex, "double");//旋转
              attachment.ros =ros;
          }
          if (version >= 2)
          {
              let scale =[];
              for(let n=0;n<3;n++){
                  scale[n] =ORAlAnalysis.hexAnalysis(hex, "double");//大小
              }
              attachment.scale =scale;
          }
          attachments[i] =attachment;
      }
      tooth.attachments=attachments;
      tooths[tooth.toothid] =tooth;
  }
  data.tooths =tooths;
  analysisNum = 0;
  return data;
}


ORAlAnalysis.hexAnalysis =function (hex, type) {

  var bytarr = []; 
  var hexd;
  if (type == "int") {
      for (var i = analysisNum; i <4+analysisNum; i++) {
      bytarr.push(hex[i]);
      }
      analysisNum += 4;
      hexd=    HEX.Hex2int(HEX.toHexString(bytarr.reverse()));
  }
  else if (type == "double") {   
      for (var i = analysisNum; i < analysisNum+8; i++) {
      bytarr.push(hex[i]);
      }
      analysisNum += 8;
     hexd =  HEX.fromIEEE754Double(bytarr.reverse());
  }
  else if (type == "longint") {   
      for (var i = analysisNum; i < analysisNum+8; i++) {
      bytarr.push(hex[i]);
      }
      analysisNum += 8;
      hexd =  HEX.hexToLongLong(bytarr);
  // console.info(bytarr+"::"+analysisNum);
  //  hexd =  HEX.h2d(HEX.toHexString(bytarr.reverse()));

  //  let byte= bytarr.reverse();
  //  let byteee=[];
  //  for (var i = 0; i < 4; i++) {
  //     byteee.push(byte[i]);
  //     }
  //     hexd=    HEX.Hex2int(HEX.toHexString(byteee));
  }
  return hexd;
}

export default ORAlAnalysis;
