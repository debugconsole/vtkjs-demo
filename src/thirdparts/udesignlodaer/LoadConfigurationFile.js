// @ts-nocheck
/* eslint-disable */

import ORAlAnalysis from "./OralAnalysisData.js";

var LoadConfigurationFile = {};

LoadConfigurationFile.loadingPath = function (path,action) {
  var data;
  var str = path.split('.');
  if(str[str.length-1]=="dat"){
    data= LoadConfigurationFile.ajax(path,action);
  }else if(str[str.length-1]=="json"){

  }
  return data;
}

LoadConfigurationFile.ajax = function(path,action) {
  //1.声明异步请求对象：
  var xmlHttp = null;
  if (window.ActiveXObject) {
      // IE6, IE5 浏览器执行代码
      xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
  } else if (window.XMLHttpRequest) {
      // IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
      xmlHttp = new XMLHttpRequest();
  }
  //2.如果实例化成功，就调用open（）方法：
  if (xmlHttp != null) {
     // xmlHttp.open("get", "DownArchCustomStaging_1.dat", true);
     xmlHttp.open("get", path, true);
     // xmlHttp.responseType = 'text';
      xmlHttp.send();
    xmlHttp.responseType = 'blob';
     xmlHttp.onreadystatechange = function() {
          if (xmlHttp.readyState == 4) { //4表示执行完成
              if (xmlHttp.status == 200) { //200表示执行成功
                  var fr = new FileReader();
                  fr.readAsArrayBuffer(xmlHttp.response); //二进制转换成text
              fr.onload = function (e) {  //转换完成后，调用onload方法
                  var result = fr.result;  //result 转换的结果
        
                  var byts = new Uint8Array(this.result); 
                  var bytarr = []; 
                  for (var i = 0; i < byts.length; i++) {
                  bytarr.push(byts[i]);
                  }
                var data=ORAlAnalysis.showAnalysis(bytarr);
                action(data);
              }
            }
          }
      }              
  }
}
export default LoadConfigurationFile;