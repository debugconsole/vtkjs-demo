import * as vtkMacro from 'vtk.js/Sources/macro'
import ORAlAnalysis from './OralAnalysisData'

export default class UDatLoader {

    content?: ArrayBuffer;
  
    public static newInstance(): UDatLoader{
      let inst = new UDatLoader();
      vtkMacro.obj(inst,inst);
      vtkMacro.algo(inst,inst,0,1);
      return inst;
    }
  
    public getClassName():string{
      return 'UDatLoader'
    }
  
    async parseAsArrayBuffer(arrayBuffer?:ArrayBuffer){
      
        console.log('aaaaa:',arrayBuffer)

        if(!arrayBuffer){
            return;
        }

        this.content = arrayBuffer;
        let int8a = new Int8Array(this.content);
        console.log('=====>11',int8a);
        let data=ORAlAnalysis.showAnalysis(int8a);
        console.log('=====>22',data);

    //   //read data from file of ctm
    //   this.content = arrayBuffer;
    //   var int8a = new Int8Array(this.content)
    //   let stream = new CTM.Stream(int8a);
    //   let file:any = new CTM.File(stream);
    //   console.log('parse ctm file:',file);
  
    }
  
    requestData(inData, outData){
      this.parseAsArrayBuffer(this.content);
    }
  
  
  }