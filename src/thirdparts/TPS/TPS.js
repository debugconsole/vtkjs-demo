// @ts-nocheck
/* eslint-disable */

/*
 * Matrix dot product
 */

function createArray(length) {
    let arr = new Array(length || 0);
    let i = length;
    if (arguments.length > 1) {
        let args = Array.prototype.slice.call(arguments, 1);
        while (i--) { arr[length - 1 - i] = createArray.apply(this, args); }
    }
    return arr;
}

// dot = function(a, b) {
//     let n = a.length;
//     let m = b.length;
//     let p = b[0].length;

//     let ret = createArray(n, p);
//     for (let i = 0; i < n; i++) {
//         for (let j = 0; j < p; j++) {
//             let sum = 0;
//             for (let k = 0; k < m; k++) { sum += a[i][k] * b[k][j]; }

//             ret[i][j] = sum;
//         }
//     }

//     return ret;
// };

//点乘  
function dot(a, b) {
    let n = a.length;
    let m = b.length;
    let p = b[0].length;

    let ret = createArray(n, p);
    for (let i = 0; i < n; i++) {
        for (let j = 0; j < p; j++) {
            let sum = 0;
            for (let k = 0; k < m; k++) { sum += a[i][k] * b[k][j]; }

            ret[i][j] = sum;
        }
    }

    return ret;
}

//矩阵转置
function transpose(m) {
    let ret = createArray(m[0].length, m.length);

    for (let i = 0; i < m[0].length; i++) {
        for (let j = 0; j < m.length; j++) {
            ret[i][j] = m[j][i];
        }
    }

    return ret;
}
// 高斯消元法  求解函数
function solve(A, b) {
    //debugger;
    // console.log("b[0]>>", b[0]);
    if (typeof(b[0]) !== 'number') {
        let ret = [];

        for (var i = 0; i < b[0].length; i++) {
            var tmp = [];
            for (var j = 0; j < b.length; j++) {
                tmp.push(b[j][i]);
            }

            ret.push(solve(A, tmp));
        }
        //console.log("ret>>", ret);
        return transpose(ret);
    } else {
        let n = A.length;
        let A_aug = createArray(n, n + 1);

        for (var i = 0; i < n; i++) {
            for (var j = 0; j < n; j++) { A_aug[i][j] = A[i][j]; }
            if (b[i] !== undefined) {
                A_aug[i][n] = b[i];
            }
        }

        for (var i = 0; i < n; i++) {
            // Search for maximum in this column
            let max_value = 0;
            let max_row = null;

            for (var k = i; k < n; k++) {
                let current_max_value = Math.abs(A_aug[k][i]);

                if (current_max_value > max_value) {
                    max_value = current_max_value;
                    max_row = k;
                }
            }

            // Swap maximum row with current row (column by column)
            for (var k = i; k < n + 1; k++) {
                var tmp = A_aug[i][k];
                A_aug[i][k] = A_aug[max_row][k];
                A_aug[max_row][k] = tmp;
            }

            // Make all rows below this one 0 in current column
            for (k = i + 1; k < n; k++) {
                let c = -A_aug[k][i] / A_aug[i][i];
                for (var j = i; j < n + 1; j++) {
                    if (i == j) { A_aug[k][j] = 0; } else { A_aug[k][j] += c * A_aug[i][j]; }
                }
            }
        }

        // Solve equation Ax=b
        let x = new Array(n);
        for (var i = n - 1; i > -1; i--) {
            x[i] = A_aug[i][n] / A_aug[i][i];
            for (var k = i - 1; k > -1; k--) { A_aug[k][n] -= A_aug[k][i] * x[i]; }
        }
        //console.log("x>>", x);
        return x;
    }
}
//向量归一化 
function unit_vector(vector) {
    let n = vector.length;

    let norm = 0;
    for (var i = 0; i < n; i++) { norm += vector[i] * vector[i]; }
    norm = Math.sqrt(norm);

    let r = [];
    for (var i = 0; i < n; i++) { r[i] = vector[i] / norm; }

    return r;
}

// eslint-disable-next-line @typescript-eslint/camelcase
function angle_between(v1, v2, deg) {
    let v1_u = unit_vector(v1);
    let v2_u = unit_vector(v2);

    let d = 0;
    // eslint-disable-next-line @typescript-eslint/camelcase
    for (let i = 0; i < v1_u.length; i++) { d += v1_u[i] * v2_u[i]; }

    let angle = Math.acos(d);

    if (v1_u[1] < 0) { angle = 2 * Math.PI - angle; }

    angle = angle % (2 * Math.PI);

    if (deg !== 'undefined' && !deg) { return angle; } else { return angle / Math.PI * 180; }

    return angle;
}

function U(r) {
    return U2(r * r);
}
// 样条函数 
function U2(r) {
    if (r == 0) { return 0; } else { return r * Math.log(r); }
}
// 基于src 和 dst的目标函数的生成
function TPS_generate(src, dst) {
    let n = src.length;

    // K Matrix
    let K = createArray(n, n);

    for (let i = 0; i < n; i++) {
        for (let j = 0; j < n; j++) {
            let dx = src[j][0] - src[i][0];
            let dy = src[j][1] - src[i][1];
            K[i][j] = U2(dx * dx + dy * dy);
        }
    }

    // L Matrix
    let L = createArray(n + 3, n + 3);

    for (let i = 0; i < n; i++) {
        for (let j = 0; j < n; j++) { L[i][j] = K[i][j]; }
    };

    for (let i = 0; i < n; i++) {
        L[i][n] = 1;
        L[i][n + 1] = src[i][0];
        L[i][n + 2] = src[i][1];

        L[n][i] = 1;
        L[n + 1][i] = src[i][0];
        L[n + 2][i] = src[i][1];
    }

    for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 3; j++) { L[n + i][n + j] = 0; }
    }

    // V Matrix
    let V = createArray(n + 3, 2);

    for (let i = 0; i < n; i++) {
        for (let j = 0; j < 2; j++) { V[i][j] = dst[i][j]; }
    }

    for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 2; j++) { V[i + n][j] = 0; }
    }

    // Solve
    //console.log("l>>", L);
    //console.log("v>>", V);
    let Wa = solve(L, V);
    //console.log("WA", Wa);
    let W = createArray(n, 2);
    for (let i = 0; i < n; i++) {
        for (let j = 0; j < 2; j++) { W[i][j] = Wa[i][j]; }
    }

    let a = createArray(3, 2);
    for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 2; j++) { a[i][j] = Wa[i + n][j]; }
    }

    // Other calculations
    //  W(T) * K * W 
    let WKW = dot(dot(transpose(W), K), W);
    let be = 0.5 * (WKW[0][0] + WKW[1][1]);

    let surfaceratio = (a[1][0] * a[2][1]) - (a[2][0] * a[1][1]);
    let scale = Math.sqrt(Math.abs(surfaceratio));

    let mirror = surfaceratio < 0;

    let shearing = angle_between([a[1][0], a[1][1]], [a[2][0], a[2][1]], true);

    // Return
    return {
        src: src,
        dst: dst,
        linear: a,
        scale: scale,
        mirror: mirror,
        shearing: shearing,
        weights: W,
        be: be
    };
}

// eslint-disable-next-line @typescript-eslint/camelcase
function TPS_project(g, x, y) {
    // vars
    let n = g.src.length;

    if (typeof(y) === 'undefined') {
        y = x[1];
        x = x[0];
    }

    let xy = [];
    xy[0] = 1;
    xy[1] = x;
    xy[2] = y;

    // Linear part -- dot( hstack( ( 1, XY ) ), linear )
    let p = [];
    for (let i = 0; i < 2; i++) {
        let tmp = 0;
        for (let j = 0; j < 3; j++) { tmp += xy[j] * g.linear[j][i]; }

        p[i] = tmp;
    }

    // Distance with the src vector
    let dist = [];
    for (let i = 0; i < n; i++) {
        let tmp = 0;
        // sum( ( src - XY ) ** 2
        for (let j = 0; j < 2; j++) { tmp += Math.pow((g.src[i][j] - xy[j + 1]), 2); }

        // U2
        dist[i] = U2(tmp);
    }

    // Non-linear part -- dot( ... , W )
    for (let i = 0; i < 2; i++) {
        // eslint-disable-next-line no-var
        // eslint-disable-next-line no-redeclare
        for (let j = 0; j < n; j++) { p[i] += dist[j] * g.weights[j][i]; }
    }

    // Return
    return p;
}

export { createArray, TPS_project, TPS_generate, U2, U, angle_between, transpose, solve };