import {CTM} from './ctm.js'
import * as vtkMacro from 'vtk.js/Sources/macro'
import vtkConeSource from 'vtk.js/Sources/Filters/Sources/ConeSource';
import vtkPolyData from 'vtk.js/Sources/Common/DataModel/PolyData';

export default class CtmLoader {

  content?: ArrayBuffer;

  public static newInstance(): CtmLoader{
    let inst = new CtmLoader();
    vtkMacro.obj(inst,inst);
    vtkMacro.algo(inst,inst,0,1);
    return inst;
  }

  public getClassName():string{
    return 'CtmLoader'
  }

  async parseAsArrayBuffer(arrayBuffer?:ArrayBuffer){
    
    if(!arrayBuffer){
        return;
    }

    if( arrayBuffer !== this.content ){

    }else{
        return;
    }

    //read data from file of ctm
    this.content = arrayBuffer;
    var int8a = new Int8Array(this.content)
    let stream = new CTM.Stream(int8a);
    let file:any = new CTM.File(stream);
    console.log('parse ctm file:',file);


    let polydata:vtk.PolyData = vtkPolyData.newInstance();
    const vecters = file.body.vertices;
    const indices = file.body.indices;
    const cellCount = indices.length/3;

    let polys = new Uint32Array(indices.length + cellCount);

    for(let i=0;i<cellCount;i++ ){
      polys[i*4]= 3;
      polys[i*4 + 1]= indices[i*3];
      polys[i*4 + 2] = indices[i*3+1];
      polys[i*4 + 3]= indices[i*3+2];
    }
    polydata.getPoints().setData(vecters, 3);
    polydata.getPolys().setData(polys);
    // Add new output
    (this as any).output[0] = polydata;


    //-----------------
    //generate polydata
    // const coneSource = vtkConeSource.newInstance({ height: 1.0 });
    // const polydata:vtk.DataModel.VtkPolyData = coneSource.getOutputData();
    // // Add new output
    // (this as any).output[0] = polydata;


    // let nCells = polydata.getNumberOfCells();   //获取图形数据的单元数目
    // let nPolys = polydata.getNumberOfPolys();
    // let polysModel = (polydata.get() as any);
    // // console.log("几何数据（点数）:",nPoints);
    // console.log("几何数据（单元）:",nCells,nPolys,polysModel);

    // let points = (polydata as any).getPoints();
    // let polys = polydata.getPolys();
    // console.log('1111',points.getNumberOfTuples(),points.getNumberOfValues());
    // console.log('2222',polys.getCellSizes());


  }

  requestData(inData, outData){
    this.parseAsArrayBuffer(this.content);
  }


}