
declare module 'itk/readLocalFileSync'

// declare for vtk.js version 1.0.2 Coded by Zasidle.Pei
declare namespace vtk{

  /**
   * Base vtkClass which provides MTime tracking and class infrastructure
   */
  class vtkObject {
    /**
     * all properties in model
     */
    model:any;
    /**
     * Allow to check if that object was deleted (.delete() was called before).
     *
     * @returns true if delete() was previously called
     */
    isDeleted(): boolean;

    /**
     * Mark the object dirty by increasing its MTime.
     * Such action also trigger the onModified() callbacks if any was registered.
     * This naturally happens when you call any setXXX(value) with a different value.
     */
    modified(): void;

    /**
     * Method to register callback when the object is modified().
     *
     * @param callback function
     * @returns subscription object so you can easily unsubscribe later on
     */
    onModified(callback: (instance: vtkObject) => any): VtkSubscription;

    /**
     * Return the `Modified Time` which is a monotonic increasing integer
     * global for all vtkObjects.
     *
     * This allow to solve a question such as:
     *  - Is that object created/modified after another one?
     *  - Do I need to re-execute this filter, or not? ...
     *
     * @returns the global modified time
     */
    getMTime(): number;

    /**
     * Method to check if an instance is of a given class name.
     * For example such method for a vtkCellArray will return true
     * for any of the following string: ['vtkObject', 'vtkDataArray', 'vtkCellArray']
     */
    isA(className: string): boolean;

    /**
     * Return the instance class name.
     */
    getClassName(): string;

    /**
     * Generic method to set many fields at one.
     *
     * For example calling the following function
     * ```
     * changeDetected = sphereSourceInstance.set({
     *    phiResolution: 10,
     *    thetaResolution: 20,
     * });
     * ```
     * will be equivalent of calling
     * ```
     * changeDetected += sphereSourceInstance.setPhiResolution(10);
     * changeDetected += sphereSourceInstance.setThetaResolution(20);
     * changeDetected = !!changeDetected;
     * ```
     *
     * In case you provide other field names that do not belong to the instance,
     * vtkWarningMacro will be used to warn you. To disable those warning,
     * you can set `noWarning` to true.
     *
     * If `noFunction` is set to true, the field will be set directly on the model
     * without calling the `set${FieldName}()` method.
     *
     * @param map (default: {}) Object capturing the set of fieldNames and associated values to set.
     * @param noWarning (default: false) Boolean to disable any warning.
     * @param noFunctions (default: false) Boolean to skip any function execution and rely on only setting the fields on the model.
     * @return true if a change was actually performed. False otherwise when the value provided were equal to the ones already set inside the instance.
     */
    set(map?: object, noWarning?: boolean, noFunction?: boolean): boolean;

    /**
     * Extract a set of properties at once from a vtkObject.
     *
     * This can be convenient to pass a partial state of
     * one object to another.
     *
     * ```
     * cameraB.set(cameraA.get('position', 'viewUp', 'focalPoint'));
     * ```
     *
     * @param listOfKeys set of field names that you want to retrieve. If not provided, the full model get returned as a new object.
     * @returns a new object containing only the values of requested fields
     */
    get(...listOfKeys: Array<string>): object;

    /**
     * Allow to get a direct reference of a model element
     *
     * @param name of the field to extract from the instance model
     * @returns model[name]
     */
    getReferenceByName(name: string): any;

    /**
     * Dereference any internal object and remove any subscription.
     * It gives custom class to properly detach themselves from the DOM
     * or any external dependency that could prevent their deletion
     * when the GC runs.
     */
    delete(): void;

    /**
     * Try to extract a serializable (JSON) object of the given
     * instance tree.
     *
     * Such state can then be reused to clone or rebuild a full
     * vtkObject tree using the root vtk() function.
     *
     * The following example will grab mapper and dataset that are
     * beneath the vtkActor instance as well.
     *
     * ```
     * const actorStr = JSON.stringify(actor.getState());
     * const newActor = vtk(JSON.parse(actorStr));
     * ```
     */
    getState(): object;

    /**
     * Try to copy the state of the other to ourselves by just using references.
     *
     * @param other instance to copy the reference from
     * @param debug (default: false) if true feedback will be provided when mismatch happen
     */
    shallowCopy(other: vtkObject, debug?: boolean): void;
  }

  interface vtkKeyStore {
    setKey: (key: string, value: any) => void;
    getKey: (key: string) => any;
    getAllKeys: () => Array<string>;
    deleteKey: (key: string) => void;
    clearKeystore: () => void;
  }


  interface vtkOutputPort{
    ():void;
  }

  interface vtkOutputPortAccess extends vtkOutputPort{
    filter:vtkAlgorithm;
  }

  /**
   * vtkAlgorithm: setInputData(), setInputConnection(), getOutputData(), getOutputPort()
   */
  abstract class vtkAlgorithm extends vtkObject{
    /**
     * need implement
     */
    abstract requestData();

    /**
     * InputData & OutputData
     */
    setInputData(val);
    setInputConnection(val:vtkOutputPortAccess);
    addInputData();
    addInputConnection(val:vtkOutputPortAccess);
    getInputData();
    getInputConnection();

    getOutputData();
    getOutputPort():vtkOutputPortAccess;
    update();
    shouldUpdate();
  }

    /**
   * implement from macro.proxy()
   * model:
   *  proxyGroup
   *  proxyName
   *  proxyManager
   */
  interface vtkProxy extends vtkKeyStore {
    getProxyId(): string;
    getProxyGroup(): string;
    getProxyName: () => string;
    setProxyManager: (pxm: VtkProxyManager) => boolean;
    getProxyManager: () => VtkProxyManager;
  
    updateUI: (ui: object) => void;
    listProxyProperties: (groupName: string) => Array<VtkProperty>;
    updateProxyProperty: (propertyName: string, propUI: object) => void;
    activate: () => void;
    registerPropertyLinkForGC: (otherLink: VtkLink, type: string) => void;
    gcPropertyLinks(type: string): void;
  
    /**
     *
     * @param id
     * @param persistent (default: false)
     */
    getPropertyLink(id: string, persistent?: boolean): VtkLink;
  
    /**
     *
     * @param groupName (default: ROOT_GROUP_NAME)
     */
    getProperties(groupName?: string): Array<any>;
    listPropertyNames: () => Array<string>;
    getPropertyByName: (name: string) => VtkProperty;
    getPropertyDomainByName: (name: string) => VtkPropertyDomain;
  
    getProxySection: () => VtkProxySection;
    delete: () => void;
  }


  namespace Common{
    namespace Core{
      interface vtkBase64{
        toArrayBuffer(b64Str:string):ArrayBuffer;
      };
      const enum VtkDataTypes {
        VOID= '', // not sure to know what that should be
        CHAR= 'Int8Array',
        SIGNED_CHAR= 'Int8Array',
        UNSIGNED_CHAR= 'Uint8Array',
        SHORT= 'Int16Array',
        UNSIGNED_SHORT= 'Uint16Array',
        INT= 'Int32Array',
        UNSIGNED_INT= 'Uint32Array',
        FLOAT= 'Float32Array',
        DOUBLE= 'Float64Array',
      };

      const DefaultDataType = VtkDataTypes.FLOAT;

      /**
       *  const data = {};
          const dataArray = vtk(data);
          alert(dataArray.getNumberOfValues());
          alert(dataArray.getValue(10));
       */
      class vtkDataArray extends vtkObject{

        //Constructor & properties
        newInstance(initalValues?:{
          name?: string = '',
          numberOfComponents?:number = 1,
          size?:number = 0,
          dataType?: DefaultDataType,
          rangeTuple?:number[2] = [0, 0],
        }):vtkDataArray;

        // Getter & Setter 
        setName(val:string);
        setNumberOfComponents(val:number);
        
        // Public API
        getElementComponentSize();
        getComponent(tupleIdx, compIdx = 0);
        setComponent(tupleIdx, compIdx, value);
        getData();
        getRange(componentIndex = -1);
        setRange(rangeValue, componentIndex);
        setTuple(idx, tuple);
        getTuple(idx, tupleToFill = TUPLE_HOLDER);
        getTupleLocation(idx = 1);
        getNumberOfComponents();
        getNumberOfValues();
        getNumberOfTuples();
        /**
         * Type of data in the array, one of:
  
          - CHAR: 'Int8Array
          - SIGNED_CHAR: 'Int8Array
          - UNSIGNED_CHAR: 'Uint8Array
          - SHORT: 'Int16Array
          - UNSIGNED_SHORT: 'Uint16Array
          - INT: 'Int32Array
          - UNSIGNED_INT: 'Uint32Array
          - FLOAT: 'Float32Array
          - DOUBLE: 'Float64Array
         */
        getDataType();
        newClone();
        getName();
        /**
         * Sets values, size, and dataType from typedArray, triggers a dataChange. 
         * Number of components is an optional argument and defaults to 1.
         * @param typedArray 
         * @param numberOfComponents 
         */
        setData(typedArray, numberOfComponents);
        getState();
      }
      
      class vtkCellArray extends vtkDataArray{

        newInstance(initalValues?:{
          empty?:boolean = true,
          numberOfComponents?:number = 1,
          dataType?:VtkDataTypes.UNSIGNED_INT
        }):vtkCellArray;

        getNumberOfCells(recompute?:boolean);
        getCellSizes(recompute?:boolean);
        setData(typedArray);
        /**
         * Returns the point indexes at the given location as a subarray.
         */
        getCell(loc);
      }
  
      class vtkPoints extends vtkDataArray{

        //Constructor & properties
        newInstance(initalValues?:{
          empty?:boolean = true,
          numberOfComponents?:number = 3,
          dataType?:VtkDataTypes = VtkDataTypes.FLOAT,
          bounds?:number[6] = [1, -1, 1, -1, 1, -1],
        }):vtkPoints;

        setNumberOfPoints(nbPoints, dimension = 3);
        setPoint(idx, ...xyz);
        getPoint();
        getBounds():number[6];
        computeBounds():number[6];
      }

    }

    namespace DataModel{

      class vtkBoundingBox extends vtkObject {
        static newInstance(initalValues?:{
          type?:string = 'vtkBoundingBox',
          bounds?:[]= [].concat(INIT_BOUNDS),
          corners?:[]= [],
        }):vtkBoundingBox;
  
        // Getters & Setters
        getBounds():number[6];
        setBounds(val:number[6]);
  
        // Public APIs
        clone():vtkBoundingBox;
        equals(other:vtkBoundingBox);
        setMinPoint(x:number, y:number, z:number);
        setMaxPoint(x:number, y:number, z:number);
        addPoint(...xyz);
        addBounds(xMin:number, xMax:number, yMin:number, yMax:number, zMin:number, zMax:number);
        addBox(other:vtkBoundingBox);
        isValid():boolean;
        intersect(bbox):boolean;
        intersects(bbox):boolean;
        intersectPlane(origin, normal):boolean;
        containsPoint(x, y, z):boolean;
        getMinPoint():number;
        getMaxPoint():number;
        getBound(index):number;
        contains(bbox):boolean;
        getCenter():number[3];
        getLength(index):number;
        getLengths():number[3];
        getMaxLength():number;
        getDiagonalLength():number;
        reset();
        inflate(delta);
        getCorners();
        scale(sx, sy, sz):boolean;
      }

      class VtkFieldData extends vtkObject{
        initialize();
        initializeFields();
        copyStructure(other);
        getNumberOfArrays();
        getNumberOfActiveArrays();
        addArray(arr);
        removeAllArrays();
        removeArray(arrayName);
        removeArrayByIndex(arrayIdx);
        getArrays(); 
        getArray(arraySpec);
        getArrayByName(arrayName);
        getArrayWithIndex(arrayName);
        getArrayByIndex(idx);
        hasArray(arrayName);
        getArrayName(idx);
        getCopyFieldFlags();
        getFlag(arrayName);
        passData(other, fromId = -1, toId = -1);
        copyFieldOn(arrayName);
        copyFieldOff(arrayName);
        copyAllOn();
        copyAllOff();
        clearFieldFlags();
        deepCopy(other);
        copyFlags(other);
        squeeze();
        reset();
        getMTime();
        getField(ids, other);
        getArrayContainingComponent(component);
        getNumberOfComponents();
        getNumberOfTuples();
        getState();
      }

      class vtkDataSetAttributes extends VtkFieldData{
        static const AttributeTypes = {
          SCALARS: 0,
          VECTORS: 1,
          NORMALS: 2,
          TCOORDS: 3,
          TENSORS: 4,
          GLOBALIDS: 5,
          PEDIGREEIDS: 6,
          EDGEFLAG: 7,
          NUM_ATTRIBUTES: 8,
        };

        checkNumberOfComponents(x);
        setAttribute(arr, uncleanAttType);
        setActiveAttributeByName(arrayName, attType);
        setActiveAttributeByIndex(arrayIdx, uncleanAttType);
        getActiveAttribute(attType);
        removeAllArrays();
        removeArray(arrayName);
        removeArrayByIndex(arrayIdx);
        initializeAttributeCopyFlags();
        shallowCopy(other, debug);
      }

      /**
       * vtkDataSet is an abstract class that specifies an interface for dataset
      objects. vtkDataSet also provides methods to provide information about
      the data, such as center, bounding box, and representative length.

      In vtk a dataset consists of a structure (geometry and topology) and
      attribute data. The structure is defined implicitly or explicitly as
      a collection of cells. The geometry of the structure is contained in
      point coordinates plus the cell interpolation functions. The topology
      of the dataset structure is defined by cell types and how the cells
      share their defining points.

      Attribute data in vtk is either point data (data at points) or cell data
      (data at cells). Typically filters operate on point data, but some may
      operate on cell data, both cell and point data, either one, or none.
      */
     class vtkDataSet{

        // Specify how data arrays can be used by data objects
        static FieldDataTypes = {
          UNIFORM: 0, // data that does not vary over points/cells/etc.
          DATA_OBJECT_FIELD: 0, // to match VTK
        
          COORDINATE: 1, // data that specifies the location of each point
          POINT_DATA: 1, // to match VTK
        
          POINT: 2, // data defined at each point, but that does not specify the point location
          POINT_FIELD_DATA: 2, // to match VTK
        
          CELL: 3, // data defined at each cell, but that does not specify the cell
          CELL_FIELD_DATA: 3, // to match VTK
        
          VERTEX: 4, // data defined at each graph vertex, but that does not specify the graph vertex
          VERTEX_FIELD_DATA: 4, // to match VTK
        
          EDGE: 5, // data defined at each graph edge, but that does not specify the graph edge
          EDGE_FIELD_DATA: 5, // to match VTK
        
          ROW: 6, // data specifying a table row
          ROW_DATA: 6, // to match VTK
        };

        pointData:vtkDataSetAttributes;
        cellData:vtkDataSetAttributes;
        fieldData:vtkDataSetAttributes;

        // Getter && Setter
        getPointData():vtkDataSetAttributes;
        setPointData(val:vtkDataSetAttributes);
        getCellData():vtkDataSetAttributes;
        setCellData(val:vtkDataSetAttributes);
        getFieldData():vtkDataSetAttributes;
        setFieldData(val:vtkDataSetAttributes);

        shallowCopy(other, debug = false);
      }

      /**
       * vtkPointSet is an abstract class that specifies the interface for
          datasets that explicitly use "point" arrays to represent geometry.
          For example, vtkPolyData and vtkUnstructuredGrid require point arrays
          to specify point position, while vtkStructuredPoints generates point
          positions implicitly.
      */
     class vtkPointSet extends vtkDataSet{
        //Constructor & properties
        static newInstance(initalValues?:{}):vtkPointSet;
        getNumberOfPoints();
        getBounds();
        computeBounds();
        shallowCopy(other, debug = false);
        getPoints():Core.vtkPoints;
        setPoints(vtkPoints:Core.vtkPoints);
      }


      class vtkPolyData extends vtkPointSet{
        static newInstance(initalValues?:{}):vtkPolyData;

        getNumberOfCells():number;
        shallowCopy(other, debug = false);
        buildCells();
        /**
         * Create upward links from points to cells that use each point. Enables
         * topologically complex queries.
         */
        buildLinks(initialSize = 0);

        // Returns an object made of the cellType and a subarray `cellPointIds` of
        // the cell points.   
        getCellPoints(cellId);
        getPointCells(ptId);
        getCellEdgeNeighbors(cellId, point1, point2);
        getCell(cellId, cellHint = null);

        // properties
        getCells():vtkCellTypes;
        getLinks():vtkCellLinks;
        getPolys():Core.vtkCellArray;
        setPolys(val:Core.vtkCellArray);
        getLines():Core.vtkCellArray;
        setLines(val:Core.vtkCellArray);
        getVerts():Core.vtkCellArray;
        setVerts(val:Core.vtkCellArray);
        getStrips():Core.vtkCellArray;
        setStrips(val:Core.vtkCellArray);

        getNumberOfVerts():number;
        getNumberOfLines():number;
        getNumberOfPolys():number;
        getNumberOfStrips():number;


      }


    };
  }

  namespace Proxy{
    namespace Core{

      class ProxyManager{
          //core.js
          setActiveSource(source);
          setActiveView(view);
          getProxyById(id):Proxy;
          getProxyGroups():Proxy[];
          getProxyInGroup(name):Proxy;
          getSources():VtkSourceProxy[];
          getRepresentations():vtkGeometryRepresentationProxy[];
          getViews():View[];
          createProxy(group, name, options):Proxy;
          getRepresentation(source, view):vtkGeometryRepresentationProxy;
          deleteProxy(proxy);
          
          //index.js
          setProxyConfiguration(config);
          getProxyConfiguration():any;
          setActiveSource(source:VtkSourceProxy);
          getActiveSource():VtkSourceProxy;
          setActiveView(view:View);
          getActiveView():View;
    
          //properties.js
          getSections():any[];
          updateCollapseState(name,stete);
          applyChanges(changeSet);
          getLookupTable(arrayName, options):any;
          getPiecewiseFunction(arrayName, options):any;
          rescaleTransferFunctionToDataRange(arrayName, dataRange);
    
          //state.js
          loadState(state, options = {}):Promise;
          saveState(options={},userData={}):Promise;
    
          //view.js
          create3DView(options):Proxy;
          create2DView(options):Proxy;
          render(view);
          renderAllViews(blocking = false);
          setAnimationOnAllViews(enable=false);
          clearAnimations();
          autoAnimateViews(debouceTimout = 250);
          resizeAllViews();
          resetCamera(view);
          createRepresentationInAllViews(source);
          resetCameraInAllViews();
      } 

      class vtkViewProxy extends vtkProxy{
        setPresetToInteractor3D(nameOrDefinitions:string):boolean;
        setPresetToInteractor2D(nameOrDefinitions:string):boolean;
        setOrientationAxesType(type:string):void;
        registerOrientationAxis(name:string, actor):void;
        unregisterOrientationAxis(name):void; 
        listOrientationAxis():void;
        setPresetToOrientationAxes(nameOrDefinitions:string):boolean;
        setContainer(container):void;
        resize():void;
        renderLater:void;
        render(blocking = true):void;
        addRepresentation(representation):void;
        removeRepresentation(representation):void;
        resetCamera():void;
        captureImage():any;
        openCaptureImage(target='_black'):Promise<any>;
        setCornerAnnotation(corner:string, templateString:String):void;
        setCornerAnnotations(annotations:object, useTemplateString = false):void;
        updateCornerAnnotation(meta):void; 
        setAnnotationOpacity(opacity:string):void;
        setBackground(color:[number, number, number]):void;
        getBackground():[number, number, number];
        setAnimation(enable:boolean, requester?:any):void;
        updateOrientation(
            axisIndex,
            orientation,
            viewUp,
            animateSteps = 0
          ):Promise<any>;
        moveCamera(focalPoint, position, viewUp, animateSteps = 0):Promise<any>;
        resetOrientation(animateSteps = 0):Promise<any>;
        rotate(angle):void;
        focusTo():void;
        delete():void;
    
        getName():string;
        setName(name:string);
        getAnnotationOpacity();
        getCamera();
        getContainer();
        getInteractor();
        getRenderer();
        getRenderWindow();
        getRepresentation();
        getUseParallelRendering();
        getProxyId():string;
      }
      
      class vtkSourceProxy extends vtkProxy{
        setInputProxy(source);
        setInputData(ds, type?);
        setInputAlgorithm(algo, type, autoUpdate = true);
        update();
        getUpdate():any;
        delete();
        getName():string;
        getType():string;
        getDataset():vtk.PolyData;
        getAlgo():any;
        getInputProxy():VtkProxy;
        setName(name);
      }

      class vtkAbstractRepresentationProxy extends vtk.Rendering.Core.vtkProp{
        // properties 
        getInput():vtkSourceProxy;
        getMapper():any;
        getActors():any;
        getVolumes():any;

        getRescaleOnColorBy();
        setRescaleOnColorBy(val);

        // Public API
        getInputDataSet();
        getDataArray(arrayName, arrayLocation);
        getLookupTableProxy(arrayName);
        setLookupTableProxy();
        getPiecewiseFunctionProxy(arrayName);
        setPiecewiseFunctionProxy();
        rescaleTransferFunctionToDataRange(n, l, c = -1);
        isVisible();
        setVisibility(visible:boolean);
        setColorBy(arrayName, arrayLocation, componentIndex = -1);
        getColorBy();
        listDataArrays();
        updateColorByDomain();
        delete();
        getNestedProps();
        getBounds();
    
      }

    }

    namespace Representations{
      class vtkGeometryRepresentationProxy extends vtk.Proxy.Core.vtkAbstractRepresentationProxy{
        
        // PropertyMapping
        setVisibility(val:boolean);
        getVisibility():boolean;
        setColor(val:number[3]);
        getColor():number[3];
        setOpacity(val);
        getOpacity();
        setInterpolateScalarsBeforeMapping(val);
        getInterpolateScalarsBeforeMapping();
        setPointSize(val);
        getPointSize();
        setUseShadow(val);
        getUseShadow();
        setUseBounds(val);
        getUseBounds();

      }
    }


  }

  namespace Filters{
    namespace Sources{
      class vtkConeSource extends vtkAlgorithm{
        // Constructor & properties
        newInstance(initalValues?:{
          height:number = 1.0,
          radius:number = 0.5,
          resolution:number = 6,
          center:number[] = [0, 0, 0],
          direction:number[] = [1.0, 0.0, 0.0],
          capping:boolean = true,
          pointType:string = 'Float32Array',
        }):vtkConeSource;

        // Getter & Setter 
        getHeight():number;
        setHeight(val:number);
        getRadius():number;
        setRadius(val:number);
        getResolution():number;
        setResolution(val:number);
        getCapping():number;
        setCapping(val:number);

      }
    }
  }

  namespace Rendering{

    namespace Core{
      
      class vtkCamera extends vtkObject{
        orthogonalizeViewUp();
        setPosition(x, y, z);
        setFocalPoint(x, y, z);
        setDistance(d);
        computeDistance();
        dolly(amount);
        roll(angle);
        azimuth(angle);
        yaw(angle);
        elevation(angle);
        pitch(angle);
        zoom(factor);
        applyTransform(transformMat4);
        getThickness();
        setThickness(thickness);
        setThicknessFromFocalPoint(thickness);
        setRoll(angle);
        getRoll();
        setObliqueAngles(alpha, beta);
        getOrientation();
        getOrientationWXYZ();
        getFrustumPlanes(aspect);
        getCameraLightTransformMatrix();
        deepCopy(sourceCamera);
        physicalOrientationToWorldDirection(ori);
        getPhysicalToWorldMatrix(result);
        getWorldToPhysicalMatrix(result);
        computeViewParametersFromViewMatrix(vmat);
        computeViewParametersFromPhysicalMatrix(mat);
        setViewMatrix(mat);
        getViewMatrix();
        setProjectionMatrix(mat);
        getProjectionMatrix(aspect, nearz, farz);
        getCompositeProjectionMatrix(aspect, nearz, farz);
        setDirectionOfProjection(x, y, z);
        setDeviceAngles(alpha, beta, gamma, screen);
        setOrientationWXYZ(degrees, x, y, z);
        computeClippingRange(bounds);

        // Getter & Setter
        getPosition():number[3];
        setPosition(val:number[3]);

        getFocalPoint():number[3];
        setFocalPoint(val:number[3]);

        getViewUp():number[3];
        setViewUp(val:number[3]);

        getDirectionOfProjection():number[3];
        setDirectionOfProjection(val:number[3]);

        getParallelProjection():boolean;
        setParallelProjection(val:boolean);

        getUseHorizontalViewAngle():number[3];
        setUseHorizontalViewAngle(val:number[3]);

        getViewAngle():number;
        setViewAngle(val:number);

        getParallelScale():number;
        setParallelScale(val:number);

        getClippingRange():number[2];
        setClippingRange(val:number[2]);

        getParallelScale():number[2];
        setParallelScale(val:number[2]);

        getWindowCenter():number;
        setWindowCenter(val:number);

        getViewPlaneNormal():number[3];
        setViewPlaneNormal(val:number[3]);

        getUseOffAxisProjection():boolean;
        setUseOffAxisProjection(val:boolean);

        getScreenBottomLeft():number[3];
        setScreenBottomLeft(val:number[3]);

        getScreenBottomRight():number[3];
        setScreenBottomRight(val:number[3]);

        getScreenTopRight():number[3];
        setScreenTopRight(val:number[3]);

        getFreezeFocalPoint():number[3];
        setFreezeFocalPoint(val:number[3]);

        getProjectionMatrix():number[3];
        setProjectionMatrix(val:number[3]);  

        getViewMatrix():number[3];
        setViewMatrix(val:number[3]);   

        // used for world to physical transformations
        getPhysicalTranslation():number[3];
        setPhysicalTranslation(val:number[3]);

        getPhysicalScale():number[3];
        setPhysicalScale(val:number[3]);  

        getPhysicalViewUp():number[3];
        setPhysicalViewUp(val:number[3]);   

        getPhysicalViewNorth():number[3];
        setPhysicalViewNorth(val:number[3]);

      }

      // -------------------------Mapper-------------------------

      /**
       * vtkAbstractMapper is an abstract class to specify interface between data and
        graphics primitives or software rendering techniques. Subclasses of
        vtkAbstractMapper can be used for rendering 2D data, geometry, or volumetric
        data.
      */
      class vtkAbstractMapper extends vtk.vtkAlgorithm{
        clippingPlanes:any[];
        update();
        addClippingPlane();
        getNumberOfClippingPlanes();
        removeAllClippingPlanes();
        removeClippingPlane();
        getClippingPlanes();
        setClippingPlanes();
      }

      enum SlicingMode {
        NONE= -1,
        I= 0,
        J= 1,
        K= 2,
        X= 3,
        Y= 4,
        Z= 5,
      }
    
      /**
       * vtkImageMapper provides 2D image display support for vtk.
          It can be associated with a vtkImageSlice prop and placed within a Renderer.
    
          This class resolves coincident topology with the same methods as [vtkMapper](./Rendering_Core_Mapper.html).
       */
      interface vtkImageMapper{
        displayExtent: number[6],
        customDisplayExtent: number[4],
        useCustomExtents: boolean,
        slice: number,
        slicingMode: SlicingMode,
        closestIJKAxis: { ijkMode: SlicingMode, flip: boolean },
        renderToRectangle: boolean,
        sliceAtFocalPoint: boolean,
    
        getSliceAtPosition(pos);
        setSliceFromCamera(cam);
        setXSlice(id);
        setYSlice(id);
        setZSlice(id);
        setISlice(id);
        setJSlice(id);
        setKSlice(id);
        getSlicingModeNormal();
        setSlicingMode(mode);
        getClosestIJKAxis();
        getBounds();
        getBoundsForSlice(slice = model.slice, thickness = 0);
        getIsOpaque();
        intersectWithLineForPointPicking(p1, p2);
        intersectWithLineForCellPicking(p1, p2);
    
      }

      /**
       * vtkAbstractMapper3D is an abstract class to specify interface between 3D
        data and graphics primitives or software rendering techniques. Subclasses
        of vtkAbstractMapper3D can be used for rendering geometry or rendering
        volumetric data.

        This class also defines an API to support hardware clipping planes (at most
        six planes can be defined). It also provides geometric data about the input
        data it maps, such as the bounding box and center.
      */
      class vtkAbstractMapper3D extends vtkAbstractMapper{
        bounds:number[6];
        center:number[3];
        getBounds();
        getBounds(bouns);
        getCenter();
        getLength();
        getClippingPlaneInDataCoords();
      }

      /**
       * vtkMapper is an abstract class to specify interface between data and
        graphics primitives. Subclasses of vtkMapper map data through a
        lookuptable and control the creation of rendering primitives that
        interface to the graphics library. The mapping can be controlled by
        supplying a lookup table and specifying a scalar range to map data
        through.

        There are several important control mechanisms affecting the behavior of
        this object. The ScalarVisibility flag controls whether scalar data (if
        any) controls the color of the associated actor(s) that refer to the
        mapper. The ScalarMode ivar is used to determine whether scalar point data
        or cell data is used to color the object. By default, point data scalars
        are used unless there are none, then cell scalars are used. Or you can
        explicitly control whether to use point or cell scalar data. Finally, the
        mapping of scalars through the lookup table varies depending on the
        setting of the ColorMode flag. See the documentation for the appropriate
        methods for an explanation.

        Another important feature of this class is whether to use immediate mode
        rendering (ImmediateModeRenderingOn) or display list rendering
        (ImmediateModeRenderingOff). If display lists are used, a data structure
        is constructed (generally in the rendering library) which can then be
        rapidly traversed and rendered by the rendering library. The disadvantage
        of display lists is that they require additional memory which may affect
        the performance of the system.

        Another important feature of the mapper is the ability to shift the
        Z-buffer to resolve coincident topology. For example, if you'd like to
        draw a mesh with some edges a different color, and the edges lie on the
        mesh, this feature can be useful to get nice looking lines. (See the
        ResolveCoincidentTopology-related methods.)
      */
      class vtkMapper extends vtkAbstractMapper3D{
        //get
        colorCoordinates;
        colorMapColors;
        colorTextureMap;

        //getset;
        colorByArrayName;
        arrayAccessMode;
        colorMode;
        fieldDataTupleId;
        interpolateScalarsBeforeMapping;
        lookupTable;
        renderTime;
        scalarMode;
        scalarVisibility;
        static;
        useLookupTableScalarRange;
        viewSpecificProperties;
        customShaderAttributes;

        getBounds();
        setForceCompileOnly();
        createDefaultLookupTable();
        getColorModeAsString();
        setColorModeToDefault();
        setColorModeToMapScalars();
        setColorModeToDirectScalars();
        getScalarModeAsString();
        setScalarModeToDefault();
        setScalarModeToUsePointData();
        setScalarModeToUseCellData();
        setScalarModeToUsePointFieldData();
        setScalarModeToUseCellFieldData();
        setScalarModeToUseFieldData();
        getAbstractScalars();
        mapScalars();
        scalarToTextureCoordinate();
        createColorTextureCoordinates();
        mapScalarsToTexture();
        getIsOpaque();
        canUseTextureMapForColoring();
        clearColorArrays();
        getLookupTable();
        getMTime();
        getPrimitiveCount();


        getResolveCoincidentTopologyPolygonOffsetParameters();
        getResolveCoincidentTopologyLineOffsetParameters();
        getResolveCoincidentTopologyPointOffsetParameters();
        setResolveCoincidentTopologyPolygonOffsetParameters(val:any);
        setResolveCoincidentTopologyLineOffsetParameters(val:any);
        setResolveCoincidentTopologyPointOffsetParameters(val:any);
      } 

      /**
       * vtkStickMapper inherits from vtkMapper.
       */
      class vtkStickMapper extends vtkMapper{
        scaleArray: [];
        orientationArray: [];
        radius: number;
        length: number;
      }

      /**
       * vtkSphereMapper inherits from vtkMapper.
       */
      class vtkSphereMapper extends vtkMapper{
        scaleArray: [];
        radius:number;
      }

      /**
       * A volume mapper that performs ray casting on 
         the GPU using fragment programs.
      */
      class vtkVolumeMapper{
        bounds: [1, -1, 1, -1, 1, -1];
        sampleDistance: 1.0;
        imageSampleDistance: 1.0;
        maximumSamplesPerRay: 1000;
        autoAdjustSampleDistances: true;
        blendMode: BlendMode.COMPOSITE_BLEND;
        averageIPScalarRange: [-1000000.0, 1000000.0];

        getBounds();
        update();
        setBlendModeToComposite();
        setBlendModeToMaximumIntensity();
        setBlendModeToMinimumIntensity();
        setBlendModeToAverageIntensity();
        getBlendModeAsString();
      }


      /**
       * 
       */
      class vtkGlyph3DMapper extends vtkMapper{
        orient: true;
        orientationMode: OrientationModes.DIRECTION;
        orientationArray: null;
        scaling: true;
        scaleFactor: 1.0;
        scaleMode: ScaleModes.SCALE_BY_MAGNITUDE;
        scaleArray: null;
        matrixArray: null;
        normalArray: null;
        colorArray: null;

        getOrientationModeAsString();
        setOrientationModeToDirection();
        setOrientationModeToRotation();
        setOrientationModeToMatrix();
        getOrientationArrayData();
        getScaleModeAsString();
        setScaleModeToScaleByMagnitude();
        setScaleModeToScaleByComponents();
        setScaleModeToScaleByConstant();
        getScaleArrayData();
        getBounds();
        buildArrays();
        getPrimitiveCount();
      }
  




      // --------------------------------Prop--------------------------------------

      /**
       * vtkProp is an abstract superclass for any objects that can exist in a
        rendered scene (either 2D or 3D). Instances of vtkProp may respond to
        various render methods (e.g., RenderOpaqueGeometry()). vtkProp also
        defines the API for picking, LOD manipulation, and common instance
        variables that control visibility, picking, and dragging.
      */
      class vtkProp{
        /**
         * Set/Get visibility of this vtkProp. Initial value is true.
         */
        visibility:boolean;
        getVisibility():boolean;
        setVisibility(val:boolean);

        /**
         * Set/Get the pickable instance variable. This determines if the vtkProp
          can be picked (typically using the mouse). Also see dragable.
          Initial value is true.
        */
        pickable:boolean;
        getPickable():boolean;
        setPickable(val:boolean);

        dragable:boolean;
        getDragable():boolean;
        setDragable(val:boolean);

        /**
         * In case the Visibility flag is true, tell if the bounds of this prop
          should be taken into account or ignored during the computation of other
          bounding boxes, like in vtkRenderer::ResetCamera().
          Initial value is true.
        */
        useBounds:boolean;

        /**
         * Return the mtime of anything that would cause the rendered image to
          appear differently. Usually this involves checking the mtime of the
          prop plus anything else it depends on such as properties, textures
          etc.
        */
        getRedrawMTime():number;

        getMTime();
        getNestedProps();
        getActors();
        getActors2D();
        getVolumes();

        setEstimatedRenderTime();
        restoreEstimatedRenderTime();
        addEstimatedRenderTime();
        setAllocatedRenderTime();
        getSupportsSelection();
        getTextures();
        hasTexture();
        addTexture();
        removeTexture();
        removeAllTextures();

      }

      /**
       * vtkProp3D is an abstract class used to represent an entity in a rendering
        scene (i.e., vtkProp3D is a vtkProp with an associated transformation
        matrix). It handles functions related to the position, orientation and
        scaling. It combines these instance variables into one 4x4 transformation
        matrix as follows: [x y z 1] = [x y z 1] Translate(-origin) Scale(scale)
        Rot(y) Rot(x) Rot (z) Trans(origin) Trans(position). Both vtkActor and
        vtkVolume are specializations of class vtkProp. The constructor defaults
        to: origin(0,0,0) position=(0,0,0) orientation=(0,0,0), no user defined
        matrix or transform, and no texture map.
      */
      class vtkProp3D extends vtkProp{
        addPosition();
        getOrientationWXYZ();
        rotateX();
        rotateY();
        rotateZ();
        rotateWXYZ();
        setOrientation();
        setUserMatrix(matrix);
        getMatrix();
        computeMatrix();
        getCenter();
        getLength();
        getXRange();
        getYRange();
        getZRange();
        getUserMatrix();
      }

      /**
       * vtkActor is used to represent an entity in a rendering scene. It inherits
        functions related to the actors position, and orientation from
        vtkProp3D. The actor also has scaling and maintains a reference to the
        defining geometry (i.e., the mapper), rendering properties, and possibly a
        texture map. vtkActor combines these instance variables into one 4x4
        transformation matrix as follows: [x y z 1] = [x y z 1] Translate(-origin)
        Scale(scale) Rot(y) Rot(x) Rot (z) Trans(origin) Trans(position)
      */
      class vtkActor extends vtkProp3D{
        getActors();
        getIsOpaque();
        hasTranslucentPolygonalGeometry();
        getProperty();
        getBounds();
        getMTime();
        getRedrawMTime();
        getSupportsSelection();
      }

      /**
       * vtkActor2D is used to represent a 2D entity in a rendering scene. It inherits
        functions related to the actors position, and orientation from
        vtkProp. The actor also has scaling and maintains a reference to the
        defining geometry (i.e., the mapper), rendering properties, and possibly a
        texture map.
      */
      class vtkActor2D extends vtkProp{
        getActors2D();
        getIsOpaque();
        hasTranslucentPolygonalGeometry();
        getProperty();
        setDisplayPosition();
        setWidth();
        setHeight();
        getWidth();
        getHeight();
        getMTime();
        getRedrawMTime();
        getBounds();
        getActualPositionCoordinate();
        getActualPositionCoordinate2();
      }

      class vtkViewport {
        vtkWindow;
        background;
        background2;
        gradientBackground:boolean;
        viewport:[4];
        aspect:[2];
        pixelAspect:[2];
        props:[];
        actors2D:[];
        addViewProp(prop);
        hasViewProp(prop):boolean;
        getViewProps():any[];
        removeViewProp(prop);
        removeAllViewProps();
        getViewPropsWithNestedProps();
    
        addActor2D();
        removeActor2D(prop);
        getActors2D();
    
        normalizedDisplayToProjection(x, y, z):any;
        normalizedDisplayToNormalizedViewport(x, y, z):any;
        normalizedViewportToProjection(x, y, z):any;
    
        projectionToNormalizedDisplay(x,y,z):any;
        normalizedViewportToNormalizedDisplay(x,y,z):any;
        projectionToNormalizedViewport(x,y,z):any;
    
      }
    
      class vtkRenderer{
        updateCamera();
        updateLightGeometry();
        getVTKWindow():any;
        setLayer(layer);
        setActiveCamera(camera);
        makeCamera():any;
        getActiveCamera():any;
        getActiveCameraAndResetIfCreated():any;
    
        getActors();
        addActor(prop);
        removeActor(actor);
        removeAllActors();
    
        getVolumes();
        addVolume(prop);
        removeVolume(volume);
        removeAllVolumes();
    
        getLights();
        addLight(light);
        removeLight(light);
        removeAllLights();
        setLightCollection(light);
        makeLight():any;
        createLight():any;
    
        normalizedDisplayToWorld(x, y, z, aspect);
        worldToNormalizedDisplay(x, y, z, aspect);
        viewToWorld(x, y, z);
        projectionToView(x, y, z, aspect);
        worldToView(x, y, z);
        viewToProjection(x, y, z, aspect);
        computeVisiblePropBounds();
        resetCamera(bounds = null);
        resetCameraClippingRange(bounds = null);
        setRenderWindow(renderWindow);
        visibleActorCount():number;
        visibleVolumeCount():number;
        getMTime():any;
        getTransparent():any;
        isActiveCameraCreated():any;
    
        twoSidedLighting:boolean;
        lightFollowCamera:boolean;
        activeCamera:boolean;
        updateLightsGeometryToFollowCamera();
    
        allocatedRenderTime:number;
        getTimeFactor():number;
    
        computeVisiblePropBounds():any[6];
        resetCameraClippingRange(bounds=null);
        erase:boolean;
        draw:boolean;
        interactive:boolean;
        layer:number;
        renderWindow:any;
        preserveColorBuffer:boolean;
        preserveDepthBuffer:boolean;
        useDepthPeeling:boolean;
        delegate:any;
        occusionRatio:number;
        maximumNumberOfPeels:number;
        useShadows:boolean;
      }
      /**
       * Create a window for renderers to be placed into

          RenderWindow is an object that specify the behavior of a
          rendering window. A rendering window is a window in a graphical user
          interface where renderers draw their images.
       */
      class vtkRenderWindow extends vtkObject{
        // Getter & Setter
        getInteractor();
        setInteractor(val);
        getNumberOfLayers();
        setNumberOfLayers(val);
        getViews();
        setViews(val);
        getRenderers():vtkRenderer[];

        //Public APIs
        addRenderer(renderer);
        removeRenderer(renderer);
        hasRenderer(ren);
        addView(view);
        removeView(view);
        hasView(view);
        render();
        getStatistics();
        captureImages(format = 'image/png');

      }

    }

    namespace SceneGraph{
      /**
       * This is the superclass for all nodes within a VTK scene graph. It
        contains the API for a node. It supports the essential operations such
        as graph creation, state storage and traversal. Child classes adapt this
        to VTK's major rendering classes. Grandchild classes adapt those to
        for APIs of different rendering libraries.
      */
      interface VtkViewNode{
        parent: null;
        renderable: null;
        myFactory: null;
        children: [];
        visited: falsenull;
        build(prepass);
        render(prepass);
        traverse(renderPass);
        apply(renderPass, prepass);
        getViewNodeFor(dataObject);
        getFirstAncestorOfType(type);
        addMissingNode(dobj);
        addMissingNodes(dataObjs);
        prepareNodes();
        setVisited(val);
        removeUnusedNodes();
        createViewNode(dataObj);
      }

      /**
       * 
       */
      interface VtkRenderPass{
        delegates: [];
        currentOperation: null;
        preDelegateOperations: [];
        postDelegateOperations: [];
        currentParent: null;
        getOperation();
        setCurrentOperation(val);
        getTraverseOperation();
        traverse(viewNode, parent = null);
      }

      /**
       * factory that chooses vtkViewNodes to create
      
        Class tells VTK which specific vtkViewNode subclass to make when it is
        asked to make a vtkViewNode for a particular renderable. modules for
        different rendering backends are expected to use this to customize the
        set of instances for their own purposes
      */
      interface VtkViewNodeFactory{
        createNode(dataObject);
        registerOverride(className, func);
      }

    }

    namespace Misc{
      class vtkFullScreenRenderWindow extends vtkObject{
        // Constructor & properties
        newInstance(initalValues?:{
          background:number[]= [0.32, 0.34, 0.43],
          containerStyle:any= null,
          controlPanelStyle:any = null,
          listenWindowResize:boolean= true,
          resizeCallback:any= null,
          controllerVisibility:boolean= true,
        }):vtkFullScreenRenderWindow;

        // Getter & Setter
        getRenderWindow():RenderWindow;
        getRenderer():Renderer;
        getOpenGLRenderWindow();
        getInteractor();
        getRootContainer();
        getContainer();
        getControlContainer();

        // Public APIs
        setBackground(val);
        removeController();
        setControllerVisibility(visible);
        toggleControllerVisibility();
        addController(html);
        addRepresentation(representation);
        removeRepresentation(representation);
        resize();
        setResizeCallback(cb);
      }
    }
  }

  // common typenames in vtk
  type ProxyManager = vtk.Proxy.Core.ProxyManager;
  type SourceProxy = vtk.Proxy.Core.vtkSourceProxy;
  type AbstractRepresentationProxy = vtk.Proxy.Core.vtkAbstractRepresentationProxy;
  type GeometryRepresentationProxy = vtk.Proxy.Representations.vtkGeometryRepresentationProxy;
  type PolyData = vtk.Common.DataModel.vtkPolyData;

  type Renderer = vtk.Rendering.Core.vtkRenderer;
  type RenderWindow = vtk.Rendering.Core.vtkRenderWindow;
  type Mapper = vtk.Rendering.Core.vtkMapper;
  type Prop = vtk.Rendering.Core.vtkProp;
  type Prop3D = vtk.Rendering.Core.vtkProp3D;

  type FullScreenRenderWindow = vtk.Rendering.Misc.vtkFullScreenRenderWindow;

  type ConeSource = vtk.Filters.Sources.vtkConeSource;

  type Actor = vtk.Rendering.Core.vtkActor;
  
}
