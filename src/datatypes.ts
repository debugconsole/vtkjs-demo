export enum EViewTypes{
    View3D='View3D:default',
    View2D_Y = 'View2D_Y:y',
    View2D_X = 'View2D_X:x',
    View2D_Z = 'View2D_Z:z',
}

