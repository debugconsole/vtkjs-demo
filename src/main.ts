/// <reference path="./declare.d.ts" />


import Vue from 'vue'
import App from './App.vue'
import router from './router'
import createStore from './store'
import 'itk'
import 'vtk.js'
import vuetify from '@/plugins/vuetify' // path to vuetify export

// register readers
import 'paraview-glance/src/io/ParaViewGlanceReaders';
import ReaderFactory from 'paraview-glance/src/io/ReaderFactory';
import ITKReader from 'paraview-glance/externals/ITKReader';
ITKReader.registerToGlance(ReaderFactory);
import CTMReader from '@/thirdparts/ctmloader/ctmloader';
import UDatReader from '@/thirdparts/udesignlodaer/udesignloader';
ReaderFactory.registerReader({
  extension: 'mtc',
  name: 'OCTM Reader',
  vtkReader: CTMReader,
  binary: true,
});

ReaderFactory.registerReader({
  extension: 'dat',
  name: 'UDesign Data Reader',
  vtkReader: UDatReader,
  binary: true,
});


import { ProxyManagerVuePlugin } from 'paraview-glance/src/plugins';
import Config from 'paraview-glance/src/config';
import vtkProxyManager from 'vtk.js/Sources/Proxy/Core/ProxyManager';



// setup vtkProxyManager to Vue
Vue.config.productionTip = false
Vue.use(ProxyManagerVuePlugin)
const proxyConfiguration = Config.Proxy;
export const proxyManager = vtkProxyManager.newInstance({ proxyConfiguration });

Vue.prototype.itk = (window as any).itk
export const store = createStore(proxyManager);

new Vue({
  router,
  store,
  vuetify,
  proxyManager,
  render: h => h(App)
}as any).$mount('#app')

