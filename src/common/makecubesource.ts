export default 
function makeCubeSource(minx,miny,minz,maxx,maxy,maxz){
    let pldData = vtk.Common.DataModel.vtkPolyData.newInstance();
    let points = [
        minx,miny,minz ,maxx,miny,minz ,maxx,maxy,minz, minx,maxy,minz,
        maxx,miny,minz, maxx,miny,maxz ,maxx,maxy,maxz, maxx,maxy,minz,
        maxx,miny,maxz, minx,miny,maxz ,minx,maxy,maxz, maxx,maxy,maxz,
        minx,miny,maxz, minx,miny,minz ,minx,maxy,minz, minx,maxy,maxz,
        minx,maxy,minz, maxx,maxy,minz ,maxx,maxy,maxz, minx,maxy,maxz,
        minx,miny,maxz, maxx,miny,maxz ,maxx,miny,minz, minx,miny,minz 
    ]
    let numberOfPolys = 6;
    let polys = new Uint32Array(numberOfPolys * 5);
    let polyIndex = 0;
    polys[polyIndex++] = 4;
    polys[polyIndex++] = 0;
    polys[polyIndex++] = 1;
    polys[polyIndex++] = 2;
    polys[polyIndex++] = 3;

    polys[polyIndex++] = 4;
    polys[polyIndex++] = 4;
    polys[polyIndex++] = 5;
    polys[polyIndex++] = 6;
    polys[polyIndex++] = 7;

    polys[polyIndex++] = 4;
    polys[polyIndex++] = 8;
    polys[polyIndex++] = 9;
    polys[polyIndex++] = 10;
    polys[polyIndex++] = 11;

    polys[polyIndex++] = 4;
    polys[polyIndex++] = 12;
    polys[polyIndex++] = 13;
    polys[polyIndex++] = 14;
    polys[polyIndex++] = 15;

    polys[polyIndex++] = 4;
    polys[polyIndex++] = 16;
    polys[polyIndex++] = 17;
    polys[polyIndex++] = 18;
    polys[polyIndex++] = 19;

    polys[polyIndex++] = 4;
    polys[polyIndex++] = 20;
    polys[polyIndex++] = 21;
    polys[polyIndex++] = 22;
    polys[polyIndex++] = 23;

    let pointBuffer = new Float32Array(points);
    pldData.getPoints().setData(pointBuffer,3);
    pldData.getPolys().setData(polys);
    return pldData;
}