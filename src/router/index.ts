import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'
import Detail from '../views/Detail.vue'
import Login from '../views/Login.vue'
import Test from '../views/Test.vue'

Vue.use(VueRouter)


const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
   return (originalPush.call(this, location) as any).catch(err => err)
}

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Login',
    component: () => import( '../views/Login.vue')
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/uplus',
    name:'uplus',
    component:()=>import('../views/UplusDemo.vue')
  },{
    path: '/login',
    name:'login',
    component:()=>import('../views/Login.vue')
  },{
    path:'/example',
    name:'example',
    component:()=>import('../views/Example.vue'),
    children:[
      {
        path:'splitview',
        name:'splitview',
        component:()=>import('../views/examples/VtkWidget.vue'),
      },
      {
        path:'algorithm',
        name:'algorithm',
        component:()=>import('../views/examples/Algorithm.vue'),
      },
      {
        path:'filter',
        name:'filter',
        component:()=>import('../views/examples/Filter.vue'),
      }
    ]
  },{
    path :"/test",
    name:"test",
    component:()=>import('../views/Test.vue'),
  }

]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
