
import Vue from 'vue';
import { Module, VuexModule, getModule,Mutation,Action } from 'vuex-module-decorators'
import { store } from '@/main'
import JSZip from 'jszip';
import vtkDataArray from 'vtk.js/Sources/Common/Core/DataArray';
import vtkImageData from 'vtk.js/Sources/Common/DataModel/ImageData';
import cloneDeep from 'lodash/cloneDeep'

import ReaderFactory from 'paraview-glance/src/io/ReaderFactory';
import postProcessDataset from 'paraview-glance/src/io/postProcessing';

function getSupportedExtensions() {
  return ['zip', 'raw', 'glance', 'gz'].concat(
    ReaderFactory.listSupportedExtensions()
  );
}

function zipGetSupportedFiles(zip, path=null) {
  const supportedExts = getSupportedExtensions();
  const promises:any[] = [];
  zip.folder(path).forEach((relPath, file) => {
    console.log('zip.folder',relPath,file)
    if (file.dir) {
      promises.push(zipGetSupportedFiles(zip, relPath));
    } else if (supportedExts.indexOf(getExtension(file.name)) > -1) {
      const splitPath = file.name.split('/');
      const baseName = splitPath[splitPath.length - 1];
      promises.push(
        zip
          .file(file.name)
          .async('blob')
          .then((blob) => new File([blob], baseName))
      );
    }
  });
  return promises;
}

function getExtension(filename) {
  const i = filename.lastIndexOf('.');
  if (i > -1) {
    return filename.substr(i + 1).toLowerCase();
  }
  return '';
}

function readRawFile(file, { dimensions, spacing, dataType }) {
  return new Promise((resolve, reject) => {
    const fio = new FileReader();
    fio.onload = function onFileReaderLoad() {
      const dataset = vtkImageData.newInstance({
        spacing,
        extent: [
          0,
          dimensions[0] - 1,
          0,
          dimensions[1] - 1,
          0,
          dimensions[2] - 1,
        ],
      });
      const scalars = vtkDataArray.newInstance({
        name: 'Scalars',
        values: new dataType.constructor(fio.result),
      });
      dataset.getPointData().setScalars(scalars);

      resolve(dataset);
    };

    fio.onerror = (error) => reject(error);

    fio.readAsArrayBuffer(file);
  });
}

@Module({ dynamic: true, store, name: 'fileloader' })
class FileLoader extends VuexModule {

  // ----states
  public fileList = [] as any[];
  public loading = false;

  // ----mutations
  @Mutation
  private _startLoading() {
    this.loading = true;
  }

  @Mutation
  private _stopLoading() {
    this.loading = false;
  }

  @Mutation
  private _resetQueue() {
    this.fileList = [];
  }
  
  @Mutation
  private _addToFileList(files) {
    console.log('addToFileList', files);
    for (let i = 0; i < files.length; i++) {
      const fileInfo = files[i];

      const fileState = {
        // possible values: needsDownload, needsInfo, loading, ready, error
        state: 'loading',
        name: fileInfo.name,
        ext: getExtension(fileInfo.name),
        files: null as any[]|null,
        reader: null,
        extraInfo: null,
        remoteURL: null,
        withGirderToken: false,
        proxyKeys: fileInfo.proxyKeys,
      };

      if (fileInfo.type === 'dicom') {
        fileState.files = fileInfo.list;
      }
      if (fileInfo.type === 'remote') {
        Object.assign(fileState, {
          state: 'needsDownload',
          remoteURL: fileInfo.remoteURL,
          withGirderToken: !!fileInfo.withGirderToken,
        });
      }
      if (fileInfo.type === 'regular') {
        fileState.files = [fileInfo.file];
      }

      this.fileList.push(fileState);
    }
  }

  @Mutation
  private _setRemoteFile({ index, file }) {
    if (index >= 0 && index < this.fileList.length) {
      this.fileList[index].state = 'loading';
      this.fileList[index].files = [file];
    }
  }

  @Mutation 
  private _setFileReader({ index, reader }) {
    if (reader && index >= 0 && index < this.fileList.length) {
      this.fileList[index].reader = reader;
      this.fileList[index].state = 'ready';
    }
  }

  @Mutation
  private _setRawFileInfo({ index, info }) {
    if (info && index >= 0 && index < this.fileList.length) {
      this.fileList[index].extraInfo = info;
      this.fileList[index].state = 'loading';
    }
  }

  @Mutation
  private _setFileError({ index, error }) {
    if (error && index >= 0 && index < this.fileList.length) {
      this.fileList[index].error = error;
      this.fileList[index].state = 'error';
    }
  }

  @Mutation
  private _setFileNeedsInfo(index) {
    if (index >= 0 && index < this.fileList.length) {
      this.fileList[index].state = 'needsInfo';
      this.fileList[index].extraInfo = null;
    }
  }

  // ----actions
  @Action
  public async promptLocal():Promise<any> {
    const exts = getSupportedExtensions();
    return new Promise((resolve, reject) =>
      ReaderFactory.openFiles(exts, (files) => {
        this.openFiles(Array.from(files))
          .then(resolve)
          .catch(reject);
      })
    );
  }

  @Action
  public async openFiles(files) {
    await this.resetQueue();
    const zips = files.filter((f) => getExtension(f.name) === 'zip');

    if (zips.length) {
      const nonzips = files.filter((f) => getExtension(f.name) !== 'zip');
      const p = zips.map((file) =>
        JSZip.loadAsync(file).then((zip) =>
          Promise.all(zipGetSupportedFiles(zip))
        )
      );
      return Promise.all(p)
        .then((results:any) => [].concat.apply(nonzips, results))
        .then((newFileList:any) => this.openFiles(newFileList));
    }

    // split out dicom and single datasets
    // all dicom files are assumed to be from a single series
    const regularFileList:any[] = [];
    const dicomFileList:any[] = [];
    files.forEach((f) => {
      if (getExtension(f.name) === 'dcm') {
        dicomFileList.push(f);
      } else {
        regularFileList.push(f);
      }
    });

    if (dicomFileList.length) {
      const dicomFile = {
        type: 'dicom',
        name: dicomFileList[0].name, // pick first file for name
        list: dicomFileList,
      };

      this._addToFileList([dicomFile]);
    }

    this._addToFileList(
      regularFileList.map((f) => ({
        type: 'regular',
        name: f.name,
        file: f,
      }))
    );

    console.log('addtoallfiles',regularFileList)

    return this.readAllFiles();
  }

  @Action
  public async readAllFiles() {
    const readPromises:any[] = [];
    for (let i = 0; i < this.fileList.length; i++) {
      readPromises.push(this.readFileIndex(i));
    }

    return Promise.all(readPromises);
  }

  @Action
  public async readFileIndex(fileIndex) {
    const file = this.fileList[fileIndex];
    let ret = Promise.resolve();

    if (file.state === 'ready' || file.state === 'error') {
      return ret;
    }
    // console.log('readfileIndex:', cloneDeep(file));
    if (file.state === 'needsDownload' && file.remoteURL) {
      const opts = {} as any;
      // if (file.withGirderToken) {
      //   opts.headers = {
      //     'Girder-Token': girder.girderRest.token,
      //   };
      // }
      ret = ReaderFactory.downloadDataset(file.name, file.remoteURL, opts)
        .then((datasetFile) => {
          this._setRemoteFile({
            index: fileIndex,
            file: datasetFile,
          });
          // re-run ReadFileIndex on our newly downloaded file.
          return this.readFileIndex(fileIndex);
        })
        .catch(() => {
          throw new Error('Failed to download file');
        });
    } else if (file.ext === 'raw') {
      if (file.extraInfo) {
        ret =  readRawFile(file.files[0], file.extraInfo).then((ds) => {
          this._setFileReader({
            index: fileIndex,
            reader: {
              name: file.name,
              dataset: ds,
            },
          });
        });
      }
      this._setFileNeedsInfo(fileIndex);
    } else if (file.ext === 'dcm') {
      console.log('readfileIndex--load dcm');
      ret = ReaderFactory.loadFileSeries(file.files, 'dcm', file.name).then(
        (r) => {
          if (r) {
            this._setFileReader({
              index: fileIndex,
              reader: r,
            });
          }
        }
      );
    } else {
      if (file.ext === 'glance') {
        // see if there is a state file before this one
        for (let i = 0; i < fileIndex; i++) {
          const f = this.fileList[i];
          if (f.ext === 'glance') {
            const error = new Error('Cannot load multiple state files');
            this._setFileError({
              index: fileIndex,
              error,
            });
          }
          return ret;
        }
      }

      ret = ReaderFactory.loadFiles(file.files).then((r) => {
        if (r && r.length === 1) {
          this._setFileReader({
            index: fileIndex,
            reader: r[0],
          });
        }
      });
    }

    return ret.catch((error) => {
      if (error) {
        this._setFileError({
          index: fileIndex,
          error: error.message || 'File load failure',
        });
      }
    });
  }

  @Action
  async loadMeshDataToSource(proxyManager):Promise<vtk.SourceProxy[]>{
    this._startLoading();
    const readyFiles = this.fileList.filter((f) => f.state === 'ready');
    let sources = await (async ()=>{
      const otherFiles = readyFiles.filter((f) => f.ext !== 'glance');
      const regularFiles:any = [];
      for (let i = 0; i < otherFiles.length; i++) {
        const file = otherFiles[i];
        regularFiles.push(file);
      }

      const loadFiles = async (fileList) => {
        let ret:vtk.SourceProxy[] = [];
        for (let i = 0; i < fileList.length; i++) {
          
          const f = fileList[i];
          const readerBundle = {
            ...f.reader,
            metadata: f.reader.metadata || {},
          };

          if (f.remoteURL) {
            Object.assign(readerBundle.metadata, { url: f.remoteURL });
          }

          const meta = f.proxyKeys && f.proxyKeys.meta;
          if (meta) {
            const { reader, dataset } = readerBundle;
            const ds =
              reader && reader.getOutputData
                ? reader.getOutputData()
                : dataset;
            Object.assign(readerBundle, {
              // use dataset instead of reader
              dataset: postProcessDataset(ds, meta),
              reader: null,
            });
          }

          const {reader,sourceType,dataset,name,metadata} = readerBundle;

          if (reader || dataset) {
            const needSource =
              (reader && reader.getOutputData) ||
              (dataset && dataset.isA && dataset.isA('vtkDataSet'));
            let proxyName = 'TrivialProducer';
            const source = needSource
            ? proxyManager.createProxy('Sources', proxyName, {
                name,
                ...metadata,
              })
            : null;
            if (dataset && dataset.isA && dataset.isA('vtkDataSet')) {
              source.setInputData(dataset, sourceType);
            } else if (reader && reader.getOutputData) {
              source.setInputAlgorithm(reader, sourceType);
            } else if (reader && reader.setProxyManager) {
              reader.setProxyManager(proxyManager);
            } else {
              console.error(`No proper reader handler was found for ${name}`);
            }  
            if (source) {
              ret.push(source);
            }
          }
            
          
          // const sources = ReaderFactory.registerReadersToProxyManager(
          //   [{ ...readerBundle, proxyKeys: f.proxyKeys }],
          //   store.state.proxyManager
          // );
          // ret = ret.concat(sources.filter(Boolean));
        }

        return ret;
      };
      return await loadFiles(regularFiles);

    })()

    this._stopLoading();
    return sources;
  }

  @Action
  load() {
    this._startLoading();
    console.log('loadstart...',this.fileList);
    const readyFiles = this.fileList.filter((f) => f.state === 'ready');
    let promise = Promise.resolve();

    // load state file first
    const stateFile = readyFiles.find((f) => f.ext === 'glance');
    // if (stateFile) {
    //   const reader = stateFile.reader.reader;
    //   promise = promise.then(() =>
    //     reader.parseAsArrayBuffer().then(() =>
    //       dispatch('restoreAppState', reader.getAppState(), {
    //         root: true,
    //       })
    //     )
    //   );
    // }

    promise = promise.then(() => {
      const otherFiles = readyFiles.filter((f) => f.ext !== 'glance');
      const regularFiles:any = [];
      const labelmapFiles:any = [];
      const measurementFiles:any = [];

      for (let i = 0; i < otherFiles.length; i++) {
        const file = otherFiles[i];
        const meta = (file.proxyKeys && file.proxyKeys.meta) || {};
        if (meta.glanceDataType === 'vtkLabelMap') {
          labelmapFiles.push(file);
        } else if (file.name.endsWith('.measurements.json')) {
          measurementFiles.push(file);
        } else {
          regularFiles.push(file);
        }
      }

      const loadFiles = (fileList) => {
        let ret = [];
        for (let i = 0; i < fileList.length; i++) {
          
          const f = fileList[i];
          const readerBundle = {
            ...f.reader,
            metadata: f.reader.metadata || {},
          };

          if (f.remoteURL) {
            Object.assign(readerBundle.metadata, { url: f.remoteURL });
          }

          const meta = f.proxyKeys && f.proxyKeys.meta;
          if (meta) {
            const { reader, dataset } = readerBundle;
            const ds =
              reader && reader.getOutputData
                ? reader.getOutputData()
                : dataset;
            Object.assign(readerBundle, {
              // use dataset instead of reader
              dataset: postProcessDataset(ds, meta),
              reader: null,
            });
          }

          // console.log('readerBundle', readerBundle);
          const sources = ReaderFactory.registerReadersToProxyManager(
            [{ ...readerBundle, proxyKeys: f.proxyKeys }],
            store.state.proxyManager
          );
          ret = ret.concat(sources.filter(Boolean));
        }
        return ret;
      };
      console.log('start loadFiles....', regularFiles);
      loadFiles(regularFiles);

      // console.log('222 labelmapFiles', labelmapFiles);
      // const loadedLabelmaps = loadFiles(labelmapFiles);

      // const sources = proxyManager
      //   .getSources()
      //   .filter((p) => p.getProxyName() === 'TrivialProducer');

      // console.log('333 load source:', sources);
      // // attach labelmaps to most recently loaded image
      // if (sources[sources.length - 1]) {
      //   const lastSourcePID = sources[sources.length - 1].getProxyId();
      //   for (let i = 0; i < loadedLabelmaps.length; i++) {
      //     const lmProxy = loadedLabelmaps[i];
      //     dispatch(
      //       'widgets/addLabelmapToImage',
      //       {
      //         imageId: lastSourcePID,
      //         labelmapId: lmProxy.getProxyId(),
      //       },
      //       { root: true }
      //     ).then(() =>
      //       dispatch(
      //         'widgets/setLabelmapState',
      //         {
      //           labelmapId: lmProxy.getProxyId(),
      //           labelmapState: {
      //             selectedLabel: 1,
      //             lastColorIndex: 1,
      //           },
      //         },
      //         { root: true }
      //       )
      //     );
      //   }

      //   // attach measurements to most recently loaded image
      //   for (let i = 0; i < measurementFiles.length; i++) {
      //     const measurements = measurementFiles[
      //       i
      //     ].reader.reader.getOutputData();
      //     for (let m = 0; m < measurements.length; m++) {
      //       dispatch(
      //         'widgets/addMeasurementTool',
      //         {
      //           datasetId: lastSourcePID,
      //           componentName: measurements[m].componentName,
      //           data: measurements[m].data,
      //         },
      //         { root: true }
      //       );
      //     }
      //   }
      // }
    });

    return promise.finally(()=>this._stopLoading());
  }

  @Action
  public async resetQueue() {
    await this.context.commit('_resetQueue');
  }

}

export const FileLoaderModule = getModule(FileLoader)