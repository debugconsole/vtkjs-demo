
import Vue from 'vue';
import { Module, VuexModule, getModule,Mutation,Action,MutationAction } from 'vuex-module-decorators'
import { store } from '@/main'
import vtkWidgetManager from 'vtk.js/Sources/Widgets/Core/WidgetManager';
import {CaptureOn} from 'vtk.js/Sources/Widgets/Core/WidgetManager/Constants';
import ORAlAnalysis from '@/thirdparts/udesignlodaer//OralAnalysisData';
import parseGum from '@/thirdparts/udesignlodaer/parsegum';

import {
    DEFAULT_VIEW_TYPE,
    VIEW_TYPES,
    VIEW_ORIENTATIONS,
} from 'paraview-glance/src/components/core/VtkView/constants';
import { DEFAULT_BACKGROUND } from 'paraview-glance/src/components/core/VtkView/palette';
import { compile } from 'vue/types/umd';


interface AttachmentInfo{
  AttachmentId;
  Property;
  attachStep: number;
  removeStep: number;
  ros: number[];
  pos: number[];
  scale: [3];
  xAngle: number;
}

interface Attachment{
  attachments: AttachmentInfo[];
  size: number;
  toothid: number;
}

interface UplusResourceData{
  dataUp:any;
  dataDown:any;
  dataAttachmentUp  :Attachment[];
  dataAttachmentDown : Attachment[];
}


@Module({ dynamic: true, store, name: 'view' })
class View extends VuexModule {
  public backgroundColors= {}; // viewType -> bg
  public globalBackgroundColor= DEFAULT_BACKGROUND;
  public viewsInitialized = false;
  public viewOrder = ['View3D:default'] ; // VIEW_TYPES.map((v) => v.value);
  public visibleCount = 1;
  public viewTypeToId = {}; // viewType -> view ID
  public axisPreset= 'default';
  public uData = {} as UplusResourceData;

  @Mutation
  private _setGlobalBackground(background) {
    // if global bg color changes, then all bgs change.
    this.globalBackgroundColor = background;
    const keys = Object.keys(this.backgroundColors);
    for (let i = 0; i < keys.length; i++) {
      this.backgroundColors[keys[i]] = background;
    }
  }

  @Mutation
  private _setViewsInitialized() {
    this.viewsInitialized = true;
  }

  @Mutation 
  private _mapViewTypeToId({ viewType, viewId }) {
    Vue.set(this.viewTypeToId, viewType, viewId);
  }

  @Mutation
  private _setAxisPreset(preset) {
    this.axisPreset = preset;
  }

  @MutationAction({mutate:['visibleCount']})
  public async setVisibleCount(count) {
    return {
      visibleCount:count,
    }
  }

  @Action
  public async fetchATMSourceDataTest(cb?){

    // concat filename with atm
    let blobToFile = (blob, fileName)=>{
      blob.lastModifiedDate =new Date();
      blob.name = 'atm_'+fileName;
      return blob;
    }

    let atmIndexList = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
    let files:any[] = [];
    for (let index = 0; index < atmIndexList.length; index++) {
      let path = `./res/testdatas/AttachmentLib/${index}.stl`;
     
      let resp:Response =await fetch(path,{
        method: 'get',
        responseType: 'arraybuffer'
      }as any);
      
      let blob = await resp.blob(); 
      let file = blobToFile(blob,`${index}.stl`);
      files.push(file);
      
    }

    return files;
  }

  @Action
  public async fetchDataTest(cb?){

    let blobToFile = (blob, fileName)=>{
      blob.lastModifiedDate =new Date();
      blob.name = fileName;
      return blob;
    }
    let resp:Response =await fetch(this.resourceDir+'mtcdata.zip',{
      method: 'get',
      responseType: 'arraybuffer'
    }as any);

    let blob = await resp.blob(); 
    let file = blobToFile(blob,'mtcdata.zip');
    return file;
  }

  @Action
  public async getGumLineDataTest(){

    let resp1:Response = await fetch( this.resourceDir + 'teethgumlines_up.txt',{
      method: 'get',
      responseType: 'arraybuffer'
    }as any);
    let results1 = await resp1.text(); 
    let upDatas = results1.split('\r\n');

    let resp:Response = await fetch( this.resourceDir + 'teethgumlines_down.txt',{
      method: 'get',
      responseType: 'arraybuffer'
    }as any);
    let results = await resp.text(); 
    let downDatas = results.split('\r\n');

    return { upDatas,downDatas};
  }


  resourceDir = './res/testdatas/'//testdata1/

  @Action
  public async fetchAttachmentData(){
    let response = await fetch(this.resourceDir+'staging_final_trans_up.atm',{
      method: 'get',
      responseType: 'arraybuffer'
    }as any);

    let ab = await response.arrayBuffer(); 
    let int8a = new Int8Array(ab);
    let dataup=ORAlAnalysis.showAnalysisATM(int8a,'up');
    console.log('attachment up', dataup);
    this.uData['dataAttachmentUp'] = dataup.tooths; 

    response = await fetch(this.resourceDir+'staging_final_trans_down.atm',{
      method: 'get',
      responseType: 'arraybuffer'
    }as any);

    ab = await response.arrayBuffer(); 
    int8a = new Int8Array(ab);
    let datadown=ORAlAnalysis.showAnalysisATM(int8a,'down');
    console.log('attachment down', datadown);
    this.uData['dataAttachmentDown'] = datadown.tooths; 

  }

  @Action
  public async getUdatTest(){

    await fetch( this.resourceDir +'DownArchCustomStaging_1.dat',{
        method: 'get',
        responseType: 'arraybuffer'
    }as any).then(async (response)=>{
      console.log(response)
      let ab = await response.arrayBuffer(); 
      let int8a = new Int8Array(ab);
      let data=ORAlAnalysis.showAnalysis(int8a);
      this.uData['dataDown'] = data; 
    });

    await fetch(this.resourceDir +'UpArchCustomStaging_1.dat',{
      method: 'get',
      responseType: 'arraybuffer'
    }as any).then(async (response)=>{
      console.log(response)
      let ab = await response.arrayBuffer(); 
      let int8a = new Int8Array(ab);
      let data=ORAlAnalysis.showAnalysis(int8a);
      this.uData['dataUp'] = data;
    });

  }

  @Action 
  public initViews(){
    if (!this.viewsInitialized) {
      this._setViewsInitialized();
      console.log('init view11:', this.viewOrder);
      let defaultView:any = null;
      this.viewOrder.forEach((viewType) => {
        const [type, name] = viewType.split(':');
        const view = store.state.proxyManager.createProxy('Views', type, { name });
        // Update orientation
        const { axis, orientation, viewUp } = VIEW_ORIENTATIONS[name];
        view.getCamera().setParallelProjection(true);
        view.updateOrientation(axis, orientation, viewUp);

        // set background to transparent
        view.setBackground(0, 0, 0, 0);
        // set actual background from global bg color
        Vue.set(
          this.backgroundColors,
          viewType,
          this.globalBackgroundColor
        );

        view.setPresetToOrientationAxes('default');

        if (!view.getReferenceByName('widgetManager')) {
          const widgetManager = vtkWidgetManager.newInstance();
          // workaround for view not yet being mounted
          widgetManager.set({ useSvgLayer: false }, false, true);
          widgetManager.setRenderer(view.getRenderer());
          widgetManager.setCaptureOn(CaptureOn.MOUSE_MOVE);
          view.set({ widgetManager }, true);
        }

        if (viewType === DEFAULT_VIEW_TYPE) {
          defaultView = view;
        }

        this._mapViewTypeToId({
          viewType,
          viewId: view.getProxyId(),
        });
        console.log(`view${view.getProxyId()}:`,view.listPropertyNames())
      });

      console.log('proxyIDType:',this.viewTypeToId)

      if (defaultView) {
        defaultView.activate();
      }
    }
  }

  @Action
  public swapViews({ index, viewType }) {
    // swap target view index with viewType view
    const dstIndex = this.viewOrder.indexOf(viewType);
    const srcViewType = this.viewOrder[index];
    Vue.set(this.viewOrder, index, viewType);
    Vue.set(this.viewOrder, dstIndex, srcViewType);
    console.log('swapviews:', index, viewType, dstIndex, srcViewType);
  }

  @Action
  public setViewType({index,viewType}){
    Vue.set(this.viewOrder, index, viewType);
  }

  @Action
  public setAxisPreset(axisPreset) {
    store.state.proxyManager.getViews().forEach((view) => {
      view.setPresetToOrientationAxes(axisPreset);
    });
    this.context.commit('setAxisPreset', axisPreset);
  }

  @Action
  public setGlobalBackground( background) {
    this.context.commit('setGlobalBackground', background);
  }

}
export const ViewModule = getModule(View)