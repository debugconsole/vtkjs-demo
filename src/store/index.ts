// import Vue from 'vue'
// import Vuex from 'vuex'
import { ProxyManagerVuexPlugin } from 'paraview-glance/src/plugins';
// import files from 'paraview-glance/src/store/fileLoader';
// import views from 'paraview-glance/src/store/views';
// import widgets from 'paraview-glance/src/store/widgets';

// Vue.use(Vuex)

// export default function createStore(pxm=null){
//   let proxyManager = pxm;
//   const store = new Vuex.Store({
//     plugins: proxyManager? [ProxyManagerVuexPlugin(proxyManager)]:[],
//     state: {
//       proxyManager,
//     },
//     mutations: {
//     },
//     actions: {
//     },
//     modules: {
//       files: files(proxyManager),
//       views: views(proxyManager),
//       widgets: widgets(proxyManager),
//     }
//   })

//   return store;
// }

import Vue from 'vue';
import Vuex from 'vuex';
 
Vue.use(Vuex);

export default function createStore(pxm){
  let proxyManager = pxm;
  const store = new Vuex.Store({
    plugins: proxyManager? [ProxyManagerVuexPlugin(proxyManager)]:[],
    state: {
      proxyManager
    }
  })  // 由于passenger->dynamic: true: 是动态创建动态模块,所以不需要再次注册

  return store;
}
