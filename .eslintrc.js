module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'plugin:vue/essential',
    '@vue/standard',
    '@vue/typescript/recommended'
  ],
  parserOptions: {
    ecmaVersion: 2020
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'prefer-const':[0],
    // 强制分号
    'semi':[1,'always'],
    'no-undef':[0],
    // -----------ts规则
    // 允许any
    '@typescript-eslint/no-explicit-any':'off',
    // 空函数警告
    '@typescript-eslint/no-empty-function':'warn',
    "@typescript-eslint/indent": ["error", 2]
  }
}
