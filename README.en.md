# itkdemo

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Server configuration
```
ip:  [图片] 52.81.17.213
port: 22
username: uplus
password: ulab@2020

文件上传路径：/home/data/static
文件访问地址示例： https://52.81.17.213:9446/static/html/index.html
```